# README #

The CS and Python3 folders are the simplistic 'file synchronisers over UDP protocol' for the TIC-80 showdown of the LoveByte democompo

CS: dotNETCore implementation, client server are a singular appication.

python3: python3 implementation, Client and Server are separate.

The CS project is developed with Visual Studio 2019 Comunity edition, but the simplistic approach and little amount of files should make it easily portable to any environment.

The python3 should run almost out of the box for every environment you set up (pycharm, notepad, etc) as long as you have python3.

### What is this repository for? ###

* The Showdown sync is a tool to sync files over UDP, mainly developed for the upcoming LoveByte democompo, and its TIC-80 Showdown (tryout for a TIC-80 version of a Shader Showdown competition)

### How do I get set up? ###

* Visual Studio 2019 Community (for CS project)
* python3 for python projects

### Who do I talk to? ###

* ehm... me... not really fond of posting my email though
* the LoveByte crew