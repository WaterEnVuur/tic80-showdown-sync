﻿namespace Tic80ShowdownClientServer
{
    using System;

    class Program
    {
        static int port = -1;
        static string server = string.Empty;
        static string showdownId = string.Empty;
        static string filename = string.Empty;

        //should run interactive mode when no params
        static void Main(string[] args)
        {
            //server
            if (args.Length == 2 && args[0].ToLower().StartsWith("s"))
            {
                int.TryParse(args[1], out port);
                RunAsServer(port);
            }
            //client
            else if (args.Length == 5 && args[0].ToLower().StartsWith("c"))
            {
                server = args[1];
                int.TryParse(args[2], out port);
                showdownId = args[3];
                filename = args[4];
                RunAsClient(server, port, showdownId, filename);
            }
            //nteractive setup
            else
            {
                StartInteractive();
            }

        }

        static void GetServerParams()
        {
            Console.Write("\nServer Port: ");
            int.TryParse(Console.ReadLine(), out port);
        }

        static void GetClientParams()
        {
            Console.Write("Server ip-address: ");
            server = Console.ReadLine();
            Console.Write("Server Port: ");
            int.TryParse(Console.ReadLine(), out port);
            Console.Write("Showdown ID: ");
            showdownId = Console.ReadLine();
            Console.Write("filename: ");
            filename = Console.ReadLine();
        }

        static void StartInteractive()
        {
            Console.WriteLine("\nUsage: program.exe (server <port> || client <server-ip> <port> <showdown-id> <filename>)");
            Console.WriteLine(" as server: program.exe server <port>");
            Console.WriteLine(" as client: program.exe client <server-ip> <port> <showdown-id> <filename>\n");

            Console.WriteLine("running without (the correct amount of) params will start interactive config.");
            Console.WriteLine("invalid input will very probably just crash and/or exit the application.\n");

            while (true)
            {
                Console.Write("[S]erver, [C]lient, or [Q]uit? ");
                var input = Console.ReadLine().ToLower()[0];
                if (input == 's')
                {
                    GetServerParams();
                    RunAsServer(port);
                }
                else if (input == 'c')
                {
                    GetClientParams();
                    RunAsClient(server, port, showdownId, filename);
                }
                else if (input == 'q')
                {
                    //exits endless loop
                    break;
                }
            }

        }

        static void RunAsServer(int port)
        {
            ServerUDP server = new ServerUDP();

            int errorcode = server.Init(port);
            if (errorcode == 0)
            {
                server.Run();
            }
            else
            {
                Console.WriteLine("Could not init Server with port {0}", port);
                Console.Write("Errorcode: {0}", errorcode);
                if (errorcode == 10048)
                {
                    Console.WriteLine(" (errorcode 10048 means socket in use)");
                }
            }
        }

        static void RunAsClient(string server, int port, string showdownid, string filename)
        {
            ClientUDP client = new ClientUDP();
            client.ShowdownId = showdownid;
            client.Filename = filename;

            int errorcode = client.Init(server, port);
            if (errorcode == 0)
            {
                client.Run();
            }
            else
            {
                Console.WriteLine("Could not init Server with port {0}", port);
                Console.WriteLine("Errorcode: {0}", errorcode);
            }
        }
    }
}
